package nl.utwente.di.bookQuote;

import java.util.HashMap;
public class Quoter {
    private final HashMap<String, Double> books;

    public Quoter() {
        books = new HashMap<>();
        books.put("1", 10.0);
        books.put("2", 45.0);
        books.put("3", 20.0);
        books.put("4", 35.0);
        books.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        return books.get(isbn);
    }
}
